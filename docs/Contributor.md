# New Contributor Wizard - Contributor Documentation

### Few Key Points

- In order to build the application from the source provided in this repository or to know more about important files and locations, must read [Developer's Doc](Developer.md).

- Again, read [Developer's Doc](Developer.md) as it's very important that you understand the reason why everything at it's place.

- Always create a Fork of this repository and then start to make changes to the forked repository.

- Once you are satisfied with the changes, create a merge request describing briefly about all the changes you have made.

- Make sure the merge reqeust is atomic, ie. only trying to solve one problem at a time.

- Always write tests, documentations and changelogs (if required) with the changes you make to the project.
