# New Contributor Wizard - Developer Documentation

### How To Build

Step 1: Install [Pipenv](https://docs.pipenv.org/)

`$ pip install pipenv`

Step 2: Clone this repository to your local machine

`$ git clone https://salsa.debian.org/new-contributor-wizard-team/new-contributor-wizard`

Step 3: Change location to repository

`$ cd new-contributor-wizard`

Step 4: Install dependencies for KIVY

- For Linux

```
# Install necessary system packages
sudo apt-get install -y \
    python-pip \
    build-essential \
    git \
    python \
    python-dev \
    ffmpeg \
    libsdl2-dev \
    libsdl2-image-dev \
    libsdl2-mixer-dev \
    libsdl2-ttf-dev \
    libportmidi-dev \
    libswscale-dev \
    libavformat-dev \
    libavcodec-dev \
    zlib1g-dev

# Install gstreamer for audio, video (optional)
sudo apt-get install -y \
    libgstreamer1.0 \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good
```

- For MacOS and Windows

This project is build and tested on Linux OS (specifically Debian 9), if you would like to deploy the application to any other OS do checkout the installation process from KIVY docs for [MacOS](https://kivy.org/docs/installation/installation-osx.html) and [Windows](https://kivy.org/docs/installation/installation-windows.html).

Step 4: Install application dependencies (this might take a while, grab a cup of coffee)

`$ pipenv install`

Step 5: Run New Contributor Wizard

`$ python main.py`

### Some Important Files/Locations

- `main.py` - It contains the Root Kivy Application which is to run in order to start the GUI.

- `data` - It contains static application data which can be used by application at anytime. It helps when the machine is offine.

- `docs` - This directory should and only contain the documentations for developers, contributors and end users.

- `modules` - This directory should and only contain the application login for different modules integrated to the Root Application. For example, this should contain the source for all application logic the tutorials and tools.

- `tests` - This directory should and only contain the Test written for the application, both for application logic and GUI.

- `UI` - This directory should and only contain the `.kv` files which uses Kivy Language in order to create the widget tree. Hence, making the UI designing easier.
