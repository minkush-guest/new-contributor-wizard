import pytest
from datetime import datetime as dt

from modules.communication.sample_module import SampleCommunicationModule

@pytest.mark.incremental
class Testsample:
    def test_return_datetime_object(self):
        resultant_datetime_object = SampleCommunicationModule.return_datetime_object("3/3/2018 12:02", "%m/%d/%Y %H:%M")
        assert type(dt.now()) == type(resultant_datetime_object)

    def test_unix_time_of_communication(self):
        resultant_unix_timestamp = SampleCommunicationModule.unix_time_of_communication("3/3/2018 12:02", "%m/%d/%Y %H:%M")
        assert 1520078520 == resultant_unix_timestamp
