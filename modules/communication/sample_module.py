'''
Sample module just to showcase how pytest should be used
'''
from datetime import datetime as dt
import calendar

class SampleCommunicationModule():
    '''
    Sample class to showcase testing
    '''

    @staticmethod
    def return_datetime_object(date_time, given_format):
        '''
        Sample method to change given date time to unix timestamp
        '''
        given_datetime_object = dt.strptime(date_time, given_format)
        return given_datetime_object


    @staticmethod
    def unix_time_of_communication(date_time, given_format):
        '''
        Sample method to change given date time to unix timestamp
        '''
        given_date_time = dt.strptime(date_time, given_format)
        unix_time = calendar.timegm(given_date_time.utctimetuple())
        return unix_time
