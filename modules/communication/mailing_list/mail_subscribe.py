#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


# Submitting user email on the website

def subscribe_mailing_list(list_name, email):

    # A session object has all methods of requests API

    client = requests.Session()
    url = 'https://lists.debian.org/cgi-bin/subscribe.pl'
    client.get(url)
    login_data = {'user_email': email, 'list': list_name,
                  'action': 'Subscribe'}
    client.post(url, data=login_data, stream=True)


# Confirming subscription by replying to the mail

def reply_by_mail(reply_code):

    reply_subject = 'CONFIRM ' + reply_code  # Final Subject of reply mail
    sender = 'gsocdebian@gmail.com'  # Sender's email address (don't change)
    receiver = list_name + '-request@lists.debian.org'
    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = receiver
    msg['Subject'] = reply_subject

    body = 'Subscribe'
    msg.attach(MIMEText(body, 'plain'))

    # Connects to server using port 587

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()

    # " " contains sender's password (don't change)

    server.login(sender, 'testuser')
    text = msg.as_string()
    server.sendmail(sender, receiver, text)
    server.quit()


# For stand-alone script

if __name__ == '__main__':
    print('This is a program to automate the process of subscription to the'
          , 'mailing list.\n')

    # Listing all mailing lists options

    print('''Mailing lists:   debian-outreach
    \t\t debian-news
    \t\t''',
           '''debian-announce
    \t\t debconf-announce
    \t\t debian-devel-announce''',
           '''
    \t\t debian-events-apac
    \t\t debian-events-eu
    \t\t''',
           '''debian-events-na
    \t\t debian-events-ha
    \t\t debian-dug-in
    ''')

    list_name = input('Enter the list which you want to subscribe: ')
    email = input('Enter you email: ')  # User's email
    subscribe_mailing_list(list_name, email)
    print('\nCheck your email for further instructions!')
    reply_code = input('\nEnter the code of the subject you recieved\
    in your email: ')
    reply_by_mail(reply_code)
    print('\nCongrats, you are successfully subscribed to % s mailing list!' \
        % list_name)
    k = input('\nPress any key to exit')