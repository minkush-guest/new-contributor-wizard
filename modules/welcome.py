'''
Class for Welcome Screen
'''
from kivy.uix.gridlayout import GridLayout
from kivy.lang import Builder

Builder.load_file('./ui/welcome.kv')


class WelcomeScreen(GridLayout):
    '''
    Declaration of Welcome Screen which is the first screen to show
    after Kivy application is running
    '''

    def __init__(self, **kwargs):
        super(WelcomeScreen, self).__init__(**kwargs)
