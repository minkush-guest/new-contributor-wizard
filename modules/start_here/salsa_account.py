#!/usr/bin/python
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

# Extract the Firefox driver and place in the same folder as this file.

browser = webdriver.Firefox()

# Salsa new account registration website's URL

browser.get('https://signup.salsa.debian.org/register/guest/')


# In GUI, the email, name would already be stored from sign-up module
# Then, user only needs to enter username and password

def create_salsa_account(user_name, full_name, user_email):


    username = browser.find_element_by_id('username_prefix')
    username.send_keys(user_name)

    name = browser.find_element_by_id('name')
    name.send_keys(full_name)

    email = browser.find_element_by_id('email')
    email.send_keys(user_email)

    pwd = browser.find_element_by_id('password')
    pwd.send_keys(user_password)

    cpwd = browser.find_element_by_id('confirm')
    cpwd.send_keys(user_password)

    submit = \
        browser.find_element_by_xpath('//input[@value = "Register account"]')
    submit.click()

    browser.quit()


if __name__ == '__main__':
    user_name = input('Enter username: ')
    full_name = input('Enter Name: ')
    user_email = input('Enter email: ')
    user_password = input('Enter password: ')
    create_salsa_account(user_name, full_name, user_email)
    print('Congrats! Your new Salsa account has been created.')
    print('Check your mail for further instructions')