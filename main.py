'''
Root Kivy Application
'''
from kivy.app import App
from kivy.config import Config

from modules.welcome import WelcomeScreen


class NewContributorWizard(App):
    '''
    Declaration of Root Kivy App which contains Root Widget
    '''

    def build(self):
        return WelcomeScreen()


if __name__ == '__main__':
    '''
    Setting window width to 720px and height to 480px
    '''
    Config.set('graphics', 'width', '720')
    Config.set('graphics', 'height', '480')

    '''
    Running Kivy application and building root Widget
    '''
    NewContributorWizard().run()
